local dev,dev_spawn,dev_z4,dev_convert_all
-- dev=true
dev_spawn=dev
dev_z4=dev
-- dev_convert_all=dev

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local collision = require "necro.game.tile.Collision"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local confusion = require "necro.game.character.Confusion"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local inventory = require "necro.game.item.Inventory"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local rng = require "necro.game.system.RNG"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local team = require "necro.game.character.Team"
local voice = require "necro.audio.Voice"

local enemyPool = require "enemypool.EnemyPool"

components.register{
  spellcastMarker={},
}

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    if dev_spawn then
      object.spawn("scattermage_enemy1",-1,-1)
      object.spawn("scattermage_enemy2",-2,0)
      object.spawn("scattermage_enemy3",2,1)
    end
  end)
end
if dev_z4 then
  event.levelGenerate.add("generateProceduralLevel", {sequence=-1}, function (ev)
    -- all zones are zone 4
    local zone = 4
    ev.level = (ev.level%4) + (zone-1)*4
  end)
end

event.levelLoad.add("registerScatterMage", {order="entities"}, function(ev)
  enemyPool.registerConversion{from="Lich",to="scattermage_enemy1",probability=0.12}
  enemyPool.registerConversion{from="Lich2",to="scattermage_enemy2",probability=0.12}
  enemyPool.registerConversion{from="Lich3",to="scattermage_enemy3",probability=0.12}
  if dev_convert_all then
    enemyPool.registerConversion{from="Lich",to="scattermage_enemy1"}
    enemyPool.registerConversion{from="Lich2",to="scattermage_enemy2"}
    enemyPool.registerConversion{from="Lich3",to="scattermage_enemy3"}
  end
end)

-- enemy
local function makeEnemyTier(tier)
  customEntities.extend {
    name="enemy"..tier,
    template=customEntities.template.enemy("lich",tier),
    components={
      friendlyName={name="Scatter Mage"},
      sprite={
        texture="mods/scattermage/images/scatter-mage"..tier..".png",
      },
      initialEquipment={
        items={"scattermage_WandClumsiness"},
      },
    },
  }
end
makeEnemyTier(1)
makeEnemyTier(2)
makeEnemyTier(3)

-- weapon the enemy holds
customEntities.extend {
  name="WandClumsiness",
  template=customEntities.template.item(),
  data={
    slot="weapon",
    flyaway="Wand of Clumsiness",
  },
  components={
    friendlyName={name="Wand of Clumsiness"},
    weaponCastOnAttack={spell="scattermage_SpellcastScatter"},
    sprite={texture="gfx/necro/items/lich_staff.png"},
    weapon={},
    weaponNoDamage={},
    weaponPattern={pattern={
      clearanceMask=collision.Group.SOLID,
      tiles={{
        offset={2, 0},
        clearance={{1, 0}},
        targetFlags=attack.Flag.CHARACTER,
      }},
    }},
  },
}

-- spell the weapon casts
commonSpell.registerSpell("SpellcastScatter", {
  spellcast={},
  spellcastLinear={
    collisionMask=collision.Type.WALL,
    minDistance=1,
    maxDistance=1,
  },
  soundSpellcast={
    sound="lichCast",
  },
  friendlyName={
    name="Scatter Items",
  },
  spellcastRadial={
    -- very important! targeting doesn't work otherwise
    radius=0,
  },
  spellcastFlyaway={
    text="Scatter spell!",
  },
  spellcastScreenFlashTargets={},
  scattermage_spellcastMarker={},
})

local scatterPriority={
  "spell",
  "ring",
  "action",
  "bomb",
  "head",
  "feet",
  "torch",
  "body",
  -- "hud", --holster/backpack; might require manually removing contained items?
  -- "misc",
  "shovel",
  "weapon",
}

-- effect the spellcast has
event.spellcast.add("spellcastScatter", {order="statusEffects", filter="scattermage_spellcastMarker"}, function(ev)
  for target in spellTargeting.targetsWithComponent(ev,"inventory") do
    for _,slot in ipairs(scatterPriority) do
      local item=inventory.getItemInSlot(target,slot,1)
      if item then
        inventory.drop(item)
        break
      end
    end
  end
end)
